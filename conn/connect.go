package conn

import (
	"context"
	"fmt"
	"net"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type OptionQuery map[string]string

func (o OptionQuery) String() string {
	s := make([]string, 0)

	for k, v := range o {
		s = append(s, fmt.Sprintf("%s=%s",
			k, v,
		))
	}

	if len(s) == 0 {
		return ""
	}

	return fmt.Sprintf("?%s",
		strings.Join(s, "&"),
	)
}

type DBOpts struct {
	User string
	Pass string
	Host net.Addr
	Db   string
	Opt  OptionQuery
}

type DB struct {
	*sqlx.DB
	Opts *DBOpts
}

func Connect(opts *DBOpts, ctx context.Context) (*DB, error) {
	db, err := sqlx.ConnectContext(ctx, "mysql", fmt.Sprintf(
		"%s:%s@%s(%s)/%s%s",
		opts.User,
		opts.Pass,
		opts.Host.Network(),
		opts.Host.String(),
		opts.Db,
		opts.Opt,
	))

	if err != nil {
		return nil, err
	}

	return &DB{
		DB:   db,
		Opts: opts,
	}, nil
}
