package orm

import (
	"sync"
)

// relType describes relation type
type relType int8

// Of these three
const (
	hasOne    relType = iota << 1
	hasMany   relType = iota << 1
	belongsTo relType = iota << 1
)

// relation describes how tables are related
type relation struct {
	*table
	f   field
	on  column
	key column
}

func (r relation) getAlias(q bool) string {
	if q {
		return SqlFlavor.Quote(r.getAlias(false))
	}
	return r.f.getFieldName()
}

func (r relation) getNameAs(q bool) string {
	return r.getName(q) + " AS " + r.getAlias(q)
}

// relations holds all relation on types
type relations struct {
	rmap map[relType][]relation
	m    *sync.Mutex
}

// addRelation to collection. Requires fields to have equal type
func (r relations) addRelation(rt relType, rel relation) {
	r.m.Lock()
	defer r.m.Unlock()

	if _, ok := r.rmap[rt]; !ok {
		r.rmap[rt] = make([]relation, 0)
	}

	if rel.on.getKind() != rel.key.getKind() {
		return
	}

	r.rmap[rt] = append(r.rmap[rt], rel)
}
