package orm

// column is a column in a table
type column struct {
	field
	ref string
}

// GetName returns the name. If relation is given, it
// will return `tbl`.`col` retpr
func (c column) getName(q bool, rel ...interface{}) string {
	if len(rel) > 0 {
		switch rel[0].(type) {
		case relation:
			return ((rel[0]).(relation)).getAlias(q) + "." + c.getName(q)
		case *table:
			return ((rel[0]).(*table)).getAlias(q) + "." + c.getName(q)
		}
	}

	if q {
		return SqlFlavor.Quote(c.ref)
	}

	return c.ref
}

func (c column) GetAs(q bool, rel ...interface{}) string {
	return c.getName(q, rel) + " AS "
}

// columns is the collection of all columns in a table
type columns []column
