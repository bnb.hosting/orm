package orm

import (
	"strings"

	"git.giaever.org/bnb.hosting/orm/conn"
	"github.com/go-openapi/inflect"
)

type MappableInterface interface {
	SetDB(*conn.DB) MappableInterface
	GetDB() *conn.DB
	GetTableMapperFn() MapperFn
	GetColumnMapperFn() MapperFn
}

type MapperFn func(string) string

type Mappable struct {
	db *conn.DB `db:"omit"`
}

func (m *Mappable) SetDB(db *conn.DB) MappableInterface {
	m.db = db
	return m
}
func (m *Mappable) GetDB() *conn.DB {
	return m.db
}

func (m Mappable) GetTableMapperFn() MapperFn {
	return func(t string) string {
		s := []byte{}
		for i := 0; i < len(t); i++ {
			c := t[i]
			if c >= 'A' && c <= 'Z' {
				if i != 0 {
					s = append(s, '_')
				}
				c = c + ('a' - 'A')
			}
			s = append(s, c)
		}

		str := strings.Split((string)(s), "_")
		str[len(str)-1] = inflect.Pluralize(str[len(str)-1])
		return strings.Join(str, "_")
	}
}

func (m Mappable) GetColumnMapperFn() MapperFn {
	return func(f string) string {
		s := []byte{}
		for i := 0; i < len(f); i++ {
			c := f[i]
			if c >= 'A' && c <= 'Z' {
				if i != 0 {
					s = append(s, '_')
				}
				c = c + ('a' - 'A')
			}
			s = append(s, c)
		}
		return (string)(s)
	}
}
