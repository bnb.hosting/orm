# orm

This is a work in progress project. Not meant to be used, as only the mapping part is _«ready»_. Check back once up in your lifetime and maybe you'll find something that works.

The only usefull information you'll get from this now is just an output representation of the relation, e.g

```
User (`users`):
 - Fields
	* id:    `id`, uint64
	* email: `email`, string
 - HasMany
	UserProperty AS `properties` ON `properties`.`user_id` WITH `id`
	Plan AS `plans` ON `plans`.`user_id` WITH `id`

UserProperty (`user_properties`):
 - Fields
	* id:      `id`, uint64
	* userId:  `user_id`, uint64
	* name:    `name`, string
	* address: `address`, string
	* lat:     `lat`, float64
	* lng:     `lng`, float64
 - BelongsTo
	User AS `user` ON `user`.`id` WITH `user_id`
 - HasOne
	Plan AS `plan` ON `plan`.`user_property_id` WITH `id`

Plan (`plans`):
 - Fields
	* id:             `id`, uint64
	* userId:         `user_id`, uint64
	* userPropertyId: `user_property_id`, uint64
 - BelongsTo
	User AS `user` ON `user`.`id` WITH `user_id`
	UserProperty AS `userProperty` ON `userProperty`.`id` WITH `user_property_id`
```

If you dare to try it out, simply

```
go get -u git.giaever.org/bnb.hosting/orm
```

and import it to a file and create a table-struct, e.g
```go
import "git.giaever.org/bnb.hosting/orm"

type MyTable struct {
    orm.Mappable // And implement missing function for interface `MappableInterface`
    field string `db:"field:col_name"`
}

func init() {
    orm.Map(&MyTable{})
}
```
