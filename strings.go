package orm

import (
	"fmt"
	"strings"
)

/**
 * Just a collectiong on strings (output) methods
 * for many of the types in the package
 *
 * Moved here for better readability in source code.
 **/

func (f fieldType) String() string {
	switch f {
	case Relation:
		return "Relation"
	}
	return "Column"
}

func (t tables) String() string {
	s := make([]string, 0)
	for _, tbl := range t {
		s = append(s, tbl.String())
	}

	return strings.Join(s, "\n\n")
}

func (t *table) String() string {
	t.Lock()
	defer t.Unlock()
	return fmt.Sprintf("%s (%s):%s%s",
		t.getStructName(), t.getName(false),
		func() string { // Print columns
			s := []string{}
			cols := strings.Split(t.cols.String(), "\n")
			if len(cols) > 0 {
				s = append(s, "\n - Fields")
			}
			max := 0
			for _, col := range cols {
				col := strings.Split(col, ":")
				if len(col[0]) > max {
					max = len(col[0])
				}

				if pk := t.getPrimaryKey(); pk != nil && "* "+pk.getName(true) == col[0] {
					col[1] = col[1] + ", primary_key"
				}

				s = append(s, strings.Join(col, ":"))
			}
			for i, col := range s {
				col := strings.Split(col, ":")
				if len(col) != 2 || len(col[0]) == max {
					continue
				}
				s[i] = col[0] + ":" + strings.Repeat(" ", max-len(col[0])) + col[1]
			}
			return strings.Join(s, "\n\t")
		}(),
		func() string {
			if len(t.rels.rmap) == 0 {
				return ""
			}

			s := []string{}
			rels := strings.Split(t.rels.String(), "\n")

			for _, rel := range rels {
				xrel := strings.Split(rel, ":")
				s = append(s, " - "+xrel[0])
				for _, rel := range strings.Split(xrel[1], ",") {
					s = append(s, "\t"+strings.Trim(rel, " "))
				}
			}

			return "\n" + strings.Join(s, "\n")
		}(),
	)
}

func (c column) String() string {
	return c.getFieldName() + ": " + c.getName(true) + ", " + c.getKind().String()
}

func (cs columns) String() string {
	s := []string{}
	for _, c := range cs {
		s = append(s, c.String())
	}
	return "* " + strings.Join(s, "\n* ")
}

func (t relType) String() string {
	switch t {
	case hasOne:
		return "HasOne"
	case hasMany:
		return "HasMany"
	case belongsTo:
		return "BelongsTo"
	}
	return "Not mapped"
}

func (r *relation) String() string {
	return r.getStructName() + " » " + r.getName(true) + " ON " + r.on.getName(true) + " WITH " + r.key.getName(true)
}

func (r relations) String() string {
	r.m.Lock()
	defer r.m.Unlock()

	s := []string{}
	for t, rels := range r.rmap {

		if len(rels) == 0 {
			continue
		}

		ss := []string{}

		for _, rel := range rels {
			ss = append(ss, rel.String())
		}

		s = append(s, t.String()+":"+strings.Join(ss, ", "))
	}

	return strings.Join(s, "\n")
}
